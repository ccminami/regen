<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="model.Mutter,java.util.List" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <%//アプリケーションスコープに保存されたつぶやきリストを取得
    List<Mutter> mutterList=(List<Mutter>)application.getAttribute("mutterList");
    //リクエストスコープに保存されたつぶやきリストを取得
    String errorMsg =(String)request.getAttribute("errorMsg");


    %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>どこつぶ</title>
</head>
<body>
<h1>どこつぶメイン</h1>
<p>

<p><a href="/regen/Main">更新</a></p>
<form action="/regen/Main" method="post">
<input type="text" name="text">
<input type="submit" value="つぶやく">
</form>

<c:if test="${not empty errorMsg}">
<p>${errorMsg}</p>
</c:if>
<c:forEach var="mutter" items="${mutterList}">

<c:out value="${mutter.text}"/></p>
</c:forEach>




</body>
</html>