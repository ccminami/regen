
<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8"%>
<!--Bootstrap css  -->
<link
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
        rel="stylesheet">
<!--fontAwesome css  -->
<link rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
  <link href="https://fonts.googleapis.com/css?family=Cherry+Swash:700 rel="stylesheet">
<style>



* {
	color: white;
	font-family: 'Cherry Swash', cursive;
}

a* {
	color: white;
}
.face:hover {
padding:10px 10px 10px 10px;

}
.twi:hover {
padding:10px 10px 10px 10px;

}
.share:hover {
padding:10px 10px 10px 10px;

}
.mail:hover {
padding:10px 10px 10px 10px;

}
.tumb:hover {
top: 1px;

}
.face{position: absolute;
left: 90%; top: 6%;}
.twi{position: absolute;
left: 86.27%; top: 6.2%;}
.tumb{position: absolute;
left: 82.57%; top: 6.2%;}
.share{position: absolute;
left: 84.47%; top: 9.7%;}
.mail{position: absolute;
left: 80.77%; top: 9.7%;}

body {

	background-image: url(img/main_off.jpg);
	background-position: center center;
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: cover;
	background-color: #464646;
}

.square_btn {
    font-size:20px;
	width: 280px;
	display: inline-block;
	padding: 10px 20px;
	margin: 10px;
	border-radius: 25px;
	text-decoration: none;
	color: #FFF;
	background-image: -webkit-linear-gradient(45deg, #FFC107 0%, #ff8b5f 100%);
	background-image: linear-gradient(45deg, #FFC107 0%, #ff8b5f 100%);
	transition: .4s;
}

.square_btn:hover {
	background-image: -webkit-linear-gradient(45deg, #FFC107 0%, #f76a35 100%);
	background-image: linear-gradient(45deg, #FFC107 0%, #f76a35 100%);
}

footer {
	width: 100%;
	height: 60px;
	position: absolute;
	bottom: 20px;
}

contents {
	display: -webkit-box;
}
.card-title{position: relative;}
.card-body{background-color: rgba(255,255,255,0.5);}

.card-footer{
	width: 150px;
	display: inline-block;
	padding: 10px 20px;
	margin: 5px;
	border-radius: 25px;
	text-decoration: none;
	color: #FFF;
	background-image: -webkit-linear-gradient(45deg, #FFC107 0%, #ff8b5f 100%);
	background-image: linear-gradient(45deg, #FFC107 0%, #ff8b5f 100%);
	transition: .4s;
}

.card-footer:hover{
	background-image: -webkit-linear-gradient(45deg, #FFC107 0%, #f76a35 100%);
	background-image: linear-gradient(45deg, #FFC107 0%, #f76a35 100%);
}

h3{font-size: 20px;
color: black;}



</style>
