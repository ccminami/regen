<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">トップページ</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>

<style type="text/css">
.row{background-color: rgba(0,0,0,0.5);}
</style>

<title>${pageName}:${siteName}</title>
</head>
<body>

<!-- ページヘッダー -->
<header class="container-fruid">
                <%@ include file="/WEB-INF/include/header.jsp"%>
        </header>

<!-- ページコンテンツ -->
<article class="container my-4 py-4">
<section class="row">


<!-- 以下メインコンテンツ -->
<h1 class="col-12 border-bottom my-4">${member.name}</h1>

<div class="media col-12">
<img alt="${member.name}の画像" src="${sitePath}/img/${member.imgSrc}"
 style="max-width: 320px;">
 <div class="media-body">
 <h5 class="mt-0">${member.name}</h5>
 <p>${member.habitat}</p>
 <p>${member.comment}</p>
 </div>
</div>
</section>

<div>
<p class="text-right">
<a href="${sitePath}/member" class="btn btn-link">メンバーページに戻る</a>
</p>
</div>
</article>





<!-- ページフッター -->
<footer class="container-fruid py-4 bg-red">
<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>




</body>
</html>
