<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">Diamondback terrapin</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>



<title>${pageName}:${siteName}</title>
</head>
<body>

<!-- ページヘッダー -->

<header class="container-fruid">
                <%@ include file="/WEB-INF/include/header.jsp"%>
        </header>
<!-- ページコンテンツ -->
<article class="container my-4 py-4">
<section class="row">


<!-- 以下メインコンテンツ -->
<h1 class="col-12 border-bottom my-4">${siteName}</h1>


<!-- メンバー一覧 -->
<c:forEach var="member" items="${memberList}">
<div class="card col-3">
<img alt="${member.name}の画像" src="${sitePath}/img/${member.imgSrc}"
 class="card-img-top" style="max-width: 300px;"/>

 <div class="card-body">
 <h3 class="card-title">${member.name}</h3>

</div>

<div class="card-footer">
<a href="${sitePath}/member?id=${member.id}">about</a>
</div>

</div>


</c:forEach>
</section>
</article>


<!-- ページフッター -->
<footer class="container-fruid py-4 bg-red">
<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>




</body>
</html>

