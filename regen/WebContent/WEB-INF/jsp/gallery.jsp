<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    <%@ include file="/WEB-INF/include/common.jsp" %>
    <c:set var="pageName">Gallery</c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS -->
<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>



<title>${pageName}:${siteName}</title>
</head>
<body>

<header class="container-fruid">
                <%@ include file="/WEB-INF/include/header.jsp"%>
        </header>

<!-- ページコンテンツ -->
<article class="container my-4 py-4">
<section class="row">


<!-- 以下メインコンテンツ -->
<h1 class="col-12  my-4">${pageName}</h1>
<c:forEach var="gallery" items="${galleryList}">
<div class="col-3">
<a href="${sitePath}/img/${gallery.src}">
<img alt="画像" src="${sitePath}/img/${gallery.src}" class="img-thumbnail"/>
</a>


</div>
</c:forEach>
</section>
</article>


<style>
*{color:white;}
a*{color:white;}
h1{color:black;}
body {

   background-image: url(img/back1.jpg),url(img/back2.jpg);

  background-position: bottom 110px right 140px,bottom 94% right 40%;
   background-repeat: no-repeat;
   background-attachment: scroll;
   background-size: 12%;
   background-color: #ffffff;


}



}




</style>




<!-- ページフッター -->
<footer class="container-fruid py-4 bg-light">
<%@ include file="/WEB-INF/include/footer.jsp" %>
</footer>




</body>
</html>


