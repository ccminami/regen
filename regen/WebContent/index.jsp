<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/include/common.jsp"%>

<c:set var="pageName">トップページ</c:set>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- CSS/ -->


<jsp:include page="/WEB-INF/include/head_css.jsp"></jsp:include>

<!-- JS -->
<jsp:include page="/WEB-INF/include/head_js.jsp"></jsp:include>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="./scripts/smart-crossfade.js"></script>


<title>${pageName}:${siteName}</title>

</head>
<body>




<header class="container-fruid">
                <%@ include file="/WEB-INF/include/header.jsp"%>
        </header>

	<article class="container my-4 bg-red py-4">
		<section class="row">


			<!-- メインコンテンツ -->

			<h1 class="col-12 border-bottom my-4">${siteName}</h1>

			<div class="contents">

				<div class="col-3 text-center">
					<a href="${sitePath}/member" class="square_btn">Diamondback
						terrapin</a>
				</div>


				<div class="col-3 text-center">
					<a href="${sitePath}/gallery" class="square_btn">Terrapin
						Gallery</a>
				</div>

				<div class="col-3 text-center">
					<a href="#contents3" class="square_btn">Poison Dart Frog</a>
				</div>

			</div>

		</section>
</article>



		<!-- ページフッター -->
		<footer class="container-fruid py-4 bg-red text-primary">
			<%@ include file="/WEB-INF/include/footer.jsp"%>
		</footer>

<script>
jQuery(document).ready(function () {

		   $('.square_btn').hover(function(){
		        $("body").css('background-image', 'url(img/main2_on.png)');
		   }, function(){
		        $("body").css('background-image', 'url(img/main_off.jpg)');
		   });
		});

</script>



</body>
</html>

