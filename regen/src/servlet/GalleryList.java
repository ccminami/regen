package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Gallery;
import model.GetGalleryListLogic;

/**
 * Servlet implementation class GalleryList
 */
@WebServlet("/gallery")
public class GalleryList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//フォワード設定
		RequestDispatcher dd;
		String forwardPath ="WEB-INF/jsp/gallery.jsp";
		GetGalleryListLogic getGalleryListLogic=new GetGalleryListLogic();
		List<Gallery> galleryList=getGalleryListLogic.execute();
		request.setAttribute("galleryList", galleryList);
		dd=request.getRequestDispatcher(forwardPath);
		dd.forward(request, response);

	}

}
