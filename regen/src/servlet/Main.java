package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.GetMutterListLogic;
import model.Mutter;
import model.PostMutterLogic;

@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//つぶやきリストを取得してリクエストスコープに保存
		GetMutterListLogic getMutterListLogic=new GetMutterListLogic();
		List<Mutter> mutterList=getMutterListLogic.execute();
		request.setAttribute("mutterList", mutterList);




	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメーターの取得
		request.setCharacterEncoding("UTF-8");
		String text = request.getParameter("text");

		// 入力値チェック
		if (text != null && text.length() != 0) {



			// つぶやきをリストに追加
			Mutter mutter = new Mutter(text);
			PostMutterLogic postMutterLogic = new PostMutterLogic();
			postMutterLogic.execute(mutter);


		} else {
			// エラーメッセージをリクエストスコープに保存
			request.setAttribute("errorMsg", "コメントが入力されていません");
		}

		//つぶやきリストを取得して、リクエストスコープに保存
		GetMutterListLogic getMutterListLogic=new GetMutterListLogic();
		List<Mutter> mutterList =getMutterListLogic.execute();
		request.setAttribute("mutterList", mutterList);

		RequestDispatcher dis = request.getRequestDispatcher("/index.jsp");
		dis.forward(request, response);



	}

}
