package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.GetMemberListLogic;
import model.GetMemberLogic;
import model.Member;

/**
 * Servlet implementation class MemberList
 */
@WebServlet("/member")
public class MemberList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//フォワード設定
		RequestDispatcher dd;
		String forwardPath="";
		//パラメーターにメンバーIDが付与されているかチェック
		String id=request.getParameter("id");
		if(id==null ||id.length()==0){
			//ID付与されていない場合
			GetMemberListLogic getMemberListLogic=new GetMemberListLogic();
			//メンバーリストを取得
			List<Member> memberList=getMemberListLogic.execute();
			request.setAttribute("memberList", memberList);
			//フォワード先
			forwardPath="/WEB-INF/jsp/member_list.jsp";

		}else{
			//メンバーIDを数値で取得
			int index=Integer.parseInt(id);
			GetMemberLogic getMemberLogic=new GetMemberLogic();
			Member member=getMemberLogic.execute(index);
			request.setAttribute("member", member);
			//フォワード先
			forwardPath="/WEB-INF/jsp/member.jsp?id="+index;
		}
//フォワード処理
		dd=request.getRequestDispatcher(forwardPath);
		dd.forward(request, response);


	}


}
