package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Mutter;

public class MutterDAO {

	private final String DRIVER_NAME = "com.mysql.jdbc.Driver";
	private final String DB_URL = "jdbc:mysql://localhost:3306/";
	private final String DB_NAME = "regen";
	private final String DB_ENCODE = "?useUnicode=true&characterEncoding=utf8";
	private final String DB_USER = "root";
	private final String DB_PASS = "root";

	public List<Mutter> findAll() {
		Connection conn = null;
		List<Mutter> mutterList = new ArrayList<>();

		try {
			Class.forName(DRIVER_NAME);
			conn = DriverManager.getConnection(DB_URL + DB_NAME + DB_ENCODE, DB_USER, DB_PASS);

			// SELECT文の準備
			String sql = "SELECT ID,NAME,TEXT FROM MUTTER ORDER BY ID DESC";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			// SELECT文を実行
			ResultSet rs = pStmt.executeQuery();

			// SELECT文の結果をArraylistに格納
			while (rs.next()) {


				String text = rs.getString("TEXT");
				Mutter mutter = new Mutter(text);
				mutterList.add(mutter);

			}

		} catch (ClassNotFoundException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
					return null;
				}
			}
		}
		return mutterList;
	}

	public boolean create(Mutter mutter){
		Connection conn=null;

		try {
			conn=DriverManager.getConnection(DB_URL+DB_NAME+DB_ENCODE,DB_USER,DB_PASS);


			//INSERT文の準備
			String sql="INSERT INTO MUTTER(TEXT) VALUES(?,?)";
			PreparedStatement pStmt=conn.prepareStatement(sql);

			//INSERT文の？に使用する値を設定し完成

			pStmt.setString(0, mutter.getText());

			//INSErt文を実行
			int result=pStmt.executeUpdate();


			if(result != 1){
				return false;
			}





		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		}finally{
			//データベース切断
			if(conn != null){

				try {
					conn.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
return true;

	}






}
