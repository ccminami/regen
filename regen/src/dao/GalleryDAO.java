package dao;

import java.util.ArrayList;
import java.util.List;

import model.Gallery;

public class GalleryDAO {
	//登録した画像すべてを呼び出す
	public List<Gallery> findAll(){

	//複数画像を登録するリスト
	List<Gallery> galleryList=new ArrayList<>();

	//リストにインスタンスを登録
	galleryList.add(new Gallery(1,"あい",  "gallery1.jpg"));
	galleryList.add(new Gallery(2,"あい" , "gallery2.jpg"));
	galleryList.add(new Gallery(3,"あい" , "gallery3.jpg"));
	galleryList.add(new Gallery(4, "あい", "gallery4.jpg"));
	galleryList.add(new Gallery(5, "あい", "gallery5.jpg"));
	galleryList.add(new Gallery(6,"あい" , "gallery6.jpg"));
	galleryList.add(new Gallery(7,"あい" , "gallery7.jpg"));
		return galleryList;

	}


}
