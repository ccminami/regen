
package model;

import java.io.Serializable;

public class Gallery implements Serializable {
    private int id;
    private String name;
    private String src;


    public Gallery(){}
    /**
     * @param id
     * @param name
     * @param src

     */
    public Gallery(int id, String name, String src) {
        this.id = id;
        this.name = name;
        this.src = src;


    }



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}}