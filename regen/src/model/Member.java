package model;

import java.io.Serializable;

public class Member implements Serializable{
	private int id;
	private String name;
	private String habitat;
	private String imgSrc;
	private String comment;


	public Member(){}
	/**
	 *
	 * @param id
	 * @param name
	 * @param age
	 * @param imgSrc
	 * @param comment
	 */
	public Member(int id, String name, String habitat, String imgSrc, String comment) {
		super();
		this.id = id;
		this.name = name;
		this.habitat = habitat;
		this.imgSrc = imgSrc;
		this.comment = comment;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHabitat() {
		return habitat;
	}
	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}
	public String getImgSrc() {
		return imgSrc;
	}
	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}


}
