package model;

import java.io.Serializable;

public class Mutter implements Serializable{

	private String text;

	public Mutter(){}

	public Mutter(String text) {
		super();

		this.text = text;
	}

    public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}




}
